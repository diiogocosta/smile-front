import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import * as xml2js from 'xml2js';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { query } from '@angular/core/src/animation/dsl';

@Injectable()
export class ApiService {
  @BlockUI() blockUI: NgBlockUI;  
  pagSeguroUrl = 'https://ws.pagseguro.uol.com.br/v2/';
  proxy = 'https://meusorrisoperfeito.com/proxy.php';
  transactionCode;
  
  constructor(private http:Http) { }

  getCep(cep){
    return this.http.get('https://viacep.com.br/ws/'+cep+'/json/');    
  }

  initSession(){
    let headers = new Headers();
    headers.append('X-Proxy-URL',this.pagSeguroUrl+'sessions?email=diiogocosta@hotmail.com&token=53F4BAC3050E41248A67E91660F11402');        
    return this.http.post(this.proxy,{},{headers:headers});    
    //email=diiogocosta@hotmail.com&token=81B90BB1543343778C12F7920E3B5956
  }

  doPayment(data){    
    let headers = new Headers();
    headers.append('X-Proxy-URL', this.pagSeguroUrl+'transactions?email=diiogocosta@hotmail.com&token=53F4BAC3050E41248A67E91660F11402');
    headers.append('Content-Type', 'application/xml');
    return this.http.post(this.proxy,data,{headers:headers}).subscribe(res=>{
      var parseString = require('xml2js').parseString;
      var xml = (<any>res)._body;      
      var transactionCode
      parseString(xml, function (err, result) {        
       transactionCode = result.transaction.code[0];        
        console.log(transactionCode);
      });        
      this.transactionCode = transactionCode;
      this.blockUI.stop();      
    }, err => {
      this.blockUI.stop();
      var parseString = require('xml2js').parseString;
      var xml = (<any>err)._body;
      var retorno;
      parseString(xml, function (err, result) {        
        console.log(result);        
      });      
    });    
  }

  sendEmail(data){    
    // let headers = new Headers({            
    //   "enctype": "multipart/form-data"
    // });    

    let formData = new FormData();

    formData.append('content',JSON.stringify(data));

    this.http.post('https://meusorrisoperfeito.com/mail/send-mail.php',formData).
    subscribe(res=>{
      console.log(res);
    },err=>{console.log(err)});
  }



}
