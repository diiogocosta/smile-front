import { ApiService } from './api.service';
import { AppRoutes } from './app.routes';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { NgxSmartModalModule } from 'ngx-smart-modal';

import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { CheckoutComponent } from './checkout/checkout.component';

import {NgxMaskModule} from 'ngx-mask'
import { BlockUIModule } from 'ng-block-ui';

import {
  ToastrModule,
  ToastNoAnimation,
  ToastNoAnimationModule,
} from 'ngx-toastr';
import { PagseguroComponent } from './pagseguro/pagseguro.component';



@NgModule({
  declarations: [
    AppComponent,    
    LandingComponent,
    CheckoutComponent,
    PagseguroComponent,
    ],
  imports: [
    BrowserModule,
    AppRoutes,    
    FormsModule,
    ToastNoAnimationModule,
    HttpModule,         
    NgxMaskModule.forRoot(),
    NgxSmartModalModule.forRoot(),
    BlockUIModule.forRoot(),
    ToastrModule.forRoot({
      toastComponent: ToastNoAnimation,
    })
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
