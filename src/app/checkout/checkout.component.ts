import { Component, OnInit } from '@angular/core';
import { ToastrService, Toast } from 'ngx-toastr';
import { Http } from '@angular/http';
import { PagseguroComponent } from '../pagseguro/pagseguro.component';


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  private interval1;
  private interval2;    
  public hora = '00';
  public zeroStr = '';
  public min = 9;
  public sec = 59;  
  private frases = [];
  private contToast = 0;  
  private nomes = ['Silvana','Adalberto','Lucimara','Noemia','Fabiana','Pedro','Eliana','Hamilton','Natália','Tiago',
                   'Roberta','Angela','Vinícius','Diogo','Fabrícia','Maria','Lucas','Antonio','Gilvan','Letícia']

  constructor(private toastr : ToastrService, private http : Http) {    
   }
   
  ngOnDestroy(){
     clearInterval(this.interval1);
     clearInterval(this.interval2);          
  }
  ngOnInit() {       
    this.interval1 = setInterval(() => {
      if (this.min == 0 && this.sec == 0)
        return false;

      this.sec = this.sec-1;
      if (this.sec == 0){        
        if (this.min == 0) 
          return false;       

        this.sec = 59        
        this.min = this.min - 1;            
      }

      if (this.sec < 10)
        this.zeroStr = '0'
      else
        this.zeroStr = '';
    }, 1000);

    this.interval2 = setInterval(()=>{      
      if (this.contToast < 20){        
          let audio = new Audio('https://s3.amazonaws.com/alphamonetizze/vendor/plugins/notificacao/sounds/sound2.ogg');
          audio.play();
          this.toastr.success('Comprou Sorriso Perfeito Veneers',this.nomes[this.contToast],{
          progressBar:true,
          progressAnimation:'increasing',
          positionClass:'toast-bottom-left'});
        this.contToast = this.contToast +1;
      } else {
        if (this.contToast == 20){
          let audio = new Audio('https://s3.amazonaws.com/alphamonetizze/vendor/plugins/notificacao/sounds/sound2.ogg');
          audio.play();

          this.toastr.success('Pessoas estão comprando Sorriso Perfeito Veneers',(Math.floor((Math.random() * 80) + 10)).toString(),{
          progressBar:true,
          progressAnimation:'increasing',
          positionClass:'toast-bottom-left'});        
          this.contToast = this.contToast +1;
        }
      }
    },30000)
  }


  goPagamento(){   
    window.scrollTo(0, 9999);    
  }  

}
