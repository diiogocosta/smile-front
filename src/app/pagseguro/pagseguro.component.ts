import { ApiService } from './../api.service';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import * as xml2js from 'xml2js';
import { truncate } from 'fs';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'pag-seguro',
  templateUrl: './pagseguro.component.html',
  styleUrls: ['./pagseguro.component.css']
})
export class PagseguroComponent implements OnInit {    
  @BlockUI() blockUI: NgBlockUI;
  public qtVezes;
  public parcelaDef = null;
  public parcelamento = [];
  public step = 1;
  public erroCartao = false;
  public isTitular = true;  
  public pageCartao = 1;
  public valida;  
  public idSession : any;
  public imageBandeira;
  public brandOk = false;
  public dispCards = [];
  public selectedCard;
  public cpf = '';
  public dtValidade = '';
  public mesValidade = '';
  public anoValidade = '';
  public cardNumber = '';
  public brand = '';
  public cvv = '';
  public aniversario = '';
  public aniversario2 = '';  
  public documents = {document : {
    type : 'CPF',
    value : ''
  }};

  public dadosPagamento : any =
  { mode: 'default',
    currency: 'BRL',    
    receiverEmail: 'diiogocosta@hotmail.com',
    sender: {
       hash: '',       
       email: '',
       phone: {
          areaCode: '',
          number: ''
       },
       documents: {
          document: {
             type: 'CPF',
             value: ''
          }
       },       
       name: ''
    },
    items: {
       item: {
          id: '1',
          description: 'MagicVeneers',
          amount: '69.90',
          quantity: '1'
       }
    },          
    method : 'creditCard',
    creditCard: {
       token: '',
       installment: {
          quantity: '1',
          noInterestInstallmentQuantity: 3,
          value: '69.90'
       },
       billingAddress: {       
        street: '',
        number: '',
        complement: '',
        district: '',
        city: '',
        state: '',
        country: 'BRA',
        postalCode: ''
      },  
       holder: {
          name: '',
          documents: { document:{
            type : 'CPF',
            value : ''
          }
          },
          birthDate: '',
          phone: {
             areaCode: '',
             number: ''
          } 
        }    
      },
      shipping: {
        // address: {
        //   street: 'Osvaldo Valcanaia',
        //   number: '11',
        //   complement: '',
        //   district: 'Paranaguamirim',
        //   city: 'Jsoinville',
        //   state: 'SC',
        //   country: 'BRA',
        //   postalCode: '89231440'
        // },              
        addressRequired: false
     },                  
 }
  
  PagSeguroDirectPayment : any;   

  constructor(public api : ApiService) {    
   }

   getScript(url, callback) {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;
    // most browsers
    script.onload = callback;
    // IE 6 & 7    
    document.getElementsByTagName('head')[0].appendChild(script);
  }

   fecha(){
     window.location.reload(true);
   }
   
   inicializa(){
    this.qtVezes = 1;
    this.api.transactionCode = null;
    this.parcelaDef = null;
    this.parcelamento = [];
    this.step = 1;
    this.erroCartao = false;
    this.isTitular = true;  
    this.pageCartao = 1;     
    this.idSession = null;    
    this.brandOk = false;
    this.dispCards = [];
    this.selectedCard = null;
    this.cpf = '';
    this.dtValidade = '';
    this.mesValidade = '';
    this.anoValidade = '';
    this.cardNumber = '';
    this.brand = '';
    this.cvv = '';
    this.aniversario = '';
    this.aniversario2 = '';
    this.documents = {document : {
      type : 'CPF',
      value : ''
    }};

  this.dadosPagamento =
  { mode: 'default',
    currency: 'BRL',    
    receiverEmail: 'diiogocosta@hotmail.com',
    sender: {
       hash: '',       
       email: '',
       phone: {
          areaCode: '',
          number: ''
       },
       documents: {
          document: {
             type: 'CPF',
             value: ''
          }
       },       
       name: ''
    },
    items: {
       item: {
          id: '1',
          description: 'MagicVeneers',
          amount: '69.90',
          quantity: '1'
       }
    },          
    method : 'creditCard',
    creditCard: {
       token: '',
       installment: {
          quantity: '1',
          noInterestInstallmentQuantity: 3,
          value: '69.90'
       },
       billingAddress: {       
        street: '',
        number: '',
        complement: '',
        district: '',
        city: '',
        state: '',
        country: 'BRA',
        postalCode: ''
      },  
       holder: {
          name: '',
          documents: { document:{
            type : 'CPF',
            value : ''
          }
          },
          birthDate: '',
          phone: {
             areaCode: '',
             number: ''
          } 
        }    
      },
      shipping: {        
        addressRequired: false
     },                  
 }
   }

  invalidateForm(){
    this.valida = {
      aniversario : false,
      aniversario2 : false,
      cpf : false,
      nome :false,
      ddd : false,
      telefone : false,
      email : false,
      cep : false,
      cidade : false,
      uf : false,
      numero : false,
      bairro : false,
      endereco : false,      
      card : false,
      validade : false,
      cvv : false,
      titnome : false,
      titaniver : false,
      titddd : false,
      titcelular : false,
      titcpf : false
    }
  }
  
  ngOnInit() {    
    this.invalidateForm();
    this.step = 1;
    let imgsrc = './assets/images/credit-card.png';
    this.imageBandeira = {background:'url('+imgsrc+') no-repeat 1px', paddingLeft:'30px'};    
  }

  next() {         
    if (this.step == 1 && this.validaStep1()){       
      this.step = this.step+1;    
      this.email();
      return false;
    }
     

    if (this.step == 2 && this.validaStep2()){
      this.step = this.step+1;            
      if (this.isTitular){
        this.dadosPagamento.creditCard.holder.phone.areaCode = this.dadosPagamento.sender.phone.areaCode;
        this.dadosPagamento.creditCard.holder.documents.document.value = this.cpf;
        this.dadosPagamento.creditCard.holder.name = this.dadosPagamento.sender.name;
        this.dadosPagamento.creditCard.holder.phone.number = this.dadosPagamento.sender.phone.number;
        this.aniversario2 = this.aniversario;
      }
      this.getSession();      
      return false;
    }        
  } 
  
  cancel() {
    
  }

  testaDate(param) {
    let data = param.substr(0,2)+'/'+param.substr(2,2)+'/'+param.substr(4,4);
    var expReg = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/(19|20)?\d{2}$/;
    var aRet = true;
    if ((data) && (data.match(expReg)) && (data != '')) {
    var dia = data.substr(0,2);
    var mes = data.substr(3,2);
    var ano = data.substr(6,4);
    if (ano.length < 4)
      aRet = false;
    if (mes == '4' || mes == '6' || mes == '9' || mes == '11' && dia > '30') 
      aRet = false;
    else 
      if ((parseInt(ano) % 4) != 0 && parseInt(mes) == 2 && parseInt(dia) > 28) 
      aRet = false;
      else
      if ((parseInt(ano)%4) == 0 && parseInt(mes) == 2 && parseInt(dia) > 29)
        aRet = false;
    }  else 
    aRet = false;  
    return aRet;
  }

  notEmpty(value){    
    if (value)
      return true
    else 
      return false;
  }

  testaCPF(value) {
    var Soma;
    var Resto;
    Soma = 0;    
    if (!value) return false;
        
  if (value == '00000000000') return false;
  
  let i;
    
	for (i=1; i<=9; i++) Soma = Soma + parseInt(value.substring(i-1, i)) * (11 - i);
	Resto = (Soma * 10) % 11;
	
    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(value.substring(9, 10)) ) return false;
	
	Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(value.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;
	
    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(value.substring(10, 11) ) ) return false;
    return true;
}

  validaStep1(){
    let ok = true;
    this.invalidateForm();

    this.documents.document.value = this.cpf;
    this.dadosPagamento.sender.documents.document.value = this.cpf;

    if (this.testaDate(this.aniversario)) this.valida.aniversario = true; else ok = false;
    if (this.testaCPF(this.dadosPagamento.sender.documents.document.value)) this.valida.cpf = true; else ok = false;
    if (this.notEmpty(this.dadosPagamento.sender.name)) this.valida.nome = true; else ok = false;
    if (this.notEmpty(this.dadosPagamento.sender.email)) this.valida.email = true; else ok = false;
    if (this.dadosPagamento.sender.phone.areaCode.length == 2) this.valida.ddd = true; else ok = false;    
    if (this.dadosPagamento.sender.phone.number.length >= 8) this.valida.telefone = true; else ok = false;
        
    return ok;    
  }

  validaStep2(){
    let ok = true;
    this.invalidateForm();

    if (this.dadosPagamento.creditCard.billingAddress.postalCode.length == 8) this.valida.cep = true; else ok = false;
    if (this.notEmpty(this.dadosPagamento.creditCard.billingAddress.city)) this.valida.cidade = true; else ok = false
    if (this.notEmpty(this.dadosPagamento.creditCard.billingAddress.district)) this.valida.bairro = true; else ok = false
    if (this.notEmpty(this.dadosPagamento.creditCard.billingAddress.number)) this.valida.numero = true; else ok = false    
    if (this.notEmpty(this.dadosPagamento.creditCard.billingAddress.street)) this.valida.endereco = true; else ok = false
    if (this.notEmpty(this.dadosPagamento.creditCard.billingAddress.state)) this.valida.uf = true; else ok = false
    
    return ok;
  }

  getCep(){
    this.blockUI.start('Carregando Endereço');
    this.api.getCep(this.dadosPagamento.creditCard.billingAddress.postalCode).subscribe(res=>{
      let dadosEndereco = JSON.parse((<any>res)._body);
      this.blockUI.stop();
      if (dadosEndereco.cep){        
        this.dadosPagamento.creditCard.billingAddress.city = dadosEndereco.localidade;
        this.dadosPagamento.creditCard.billingAddress.street = dadosEndereco.logradouro;
        this.dadosPagamento.creditCard.billingAddress.state = dadosEndereco.uf;
        this.dadosPagamento.creditCard.billingAddress.district = dadosEndereco.bairro;
      }
    },err=>this.blockUI.stop())
  }

  getBandeira(){            
    if (this.cardNumber.length >= 6 && !this.brandOk){           
      console.log(this.cardNumber);
      this.PagSeguroDirectPayment.getBrand({
        cardBin: this.cardNumber,
        success: res =>{
          console.log(res);
          this.brandOk = true;          
          this.brand = res.brand.name;          
          this.getParcelas();
          this.dispCards.forEach(card => {            
            if (card.name == this.brand.toUpperCase()){
              this.selectedCard = card;                                                            
              this.imageBandeira = {background:'url('+this.selectedCard.image+') no-repeat 1px',paddingLeft:'50px'};          
            }                        
          });
        },
        error: err=>{
          console.log(err);
          this.brandOk = false;          
          this.selectedCard = undefined;
          let imgsrc = './assets/images/credit-card.png';
          this.imageBandeira = {background:'url('+imgsrc+') no-repeat 1px', paddingLeft:'30px'};
        }
      });
    } else {
      if (this.cardNumber.length < 6){
        this.brandOk = false;
        this.selectedCard = undefined;
        let imgsrc = './assets/images/credit-card.png';
        this.imageBandeira = {background:'url('+imgsrc+') no-repeat 1px', paddingLeft:'30px'};
      }  
    }    
  }  

  getMetodoPagamento(){
    this.PagSeguroDirectPayment.getPaymentMethods({
      amount: 100.00,
      success: res=> {
        console.log(res);
        let card;
          for (card in res.paymentMethods.CREDIT_CARD.options) {
            if (res.paymentMethods.CREDIT_CARD.options.hasOwnProperty(card)) {
              var val = res.paymentMethods.CREDIT_CARD.options[card];              
            this.dispCards.push({name:val.name, image:'https://stc.pagseguro.uol.com.br'+val.images.SMALL.path})  
            }                        
          };
      },
      error: err=> {
          console.log(err);
      }    
    });  
  }

  getSession(){ 
    if (this.idSession)
      return false;

    this.getScript('https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js',res=>{
      console.log(res);
      this.PagSeguroDirectPayment = (<any>window).PagSeguroDirectPayment;        

    })
    
    console.log('passo aqui');    
    this.api.initSession().subscribe(res=>{
      var parseString = require('xml2js').parseString;
      var xml = (<any>res)._body;
      var retorno;
      console.log(retorno);
      parseString(xml, function (err, result) {        
        console.log(result);
        retorno = result.session.id[0];        
      });      
      this.idSession = retorno;
      this.PagSeguroDirectPayment.setSessionId(this.idSession);    
      setTimeout(function(){}, 3000);
      this.getMetodoPagamento()  
    })    
  }

  validaStep3(){     
    let ok = true;
    this.invalidateForm();

    if (this.brandOk && this.cardNumber.length == 16) this.valida.card = true; else ok = false;
    if (this.cvv.length >= 3) this.valida.cvv = true; else ok = false
    if (this.dtValidade.length == 4){
      this.valida.validade = true; 
      this.mesValidade = this.dtValidade.substring(0,2);
      this.anoValidade = this.dtValidade.substring(2,4);
    } else ok = false  
    
    if (this.dadosPagamento.creditCard.holder.phone.areaCode.length == 2) this.valida.titddd = true; else ok = false;
    if (this.dadosPagamento.creditCard.holder.phone.number.length >= 8) this.valida.titcelular = true; else ok = false;    
    if (this.testaDate(this.aniversario2)) this.valida.titaniver = true; else ok = false;        
    if (this.testaCPF(this.dadosPagamento.creditCard.holder.documents.document.value)) this.valida.titcpf = true; else ok = false;            
    if (this.notEmpty(this.dadosPagamento.creditCard.holder.name)) this.valida.titnome = true; else ok = false;                    
    
    return ok;
  }

  efetivaPagamento(){        
    if (!this.valida.card || !this.valida.cvv ||  !this.valida.validade)
      return false;

    this.parcelamento.forEach(parcela => {
      if (parcela.quantity == this.qtVezes)
        this.parcelaDef = parcela;
    });

    this.dadosPagamento.creditCard.installment.quantity = this.parcelaDef.quantity
    this.dadosPagamento.creditCard.installment.value = parseFloat(this.parcelaDef.installmentAmount).toFixed(2);       

    this.blockUI.start('Processando Pagamento');
    this.dadosPagamento.sender.hash = this.PagSeguroDirectPayment.getSenderHash();
    console.log(this.dadosPagamento.sender.hash);
    this.PagSeguroDirectPayment.createCardToken({
      cardNumber: this.cardNumber,
      brand: this.brand,
      cvv: this.cvv,
      expirationMonth: this.mesValidade,
      expirationYear: '20'+this.anoValidade,
      success: res=>{        
        this.erroCartao = false;
        this.dadosPagamento.creditCard.token = res.card.token;
        this.dadosPagamento.creditCard.holder.birthDate = this.aniversario2.substr(0,2)+'/'+this.aniversario2.substr(2,2)+'/'+this.aniversario2.substr(4,4);
        console.log(this.dadosPagamento);
        
        var xml2js = require('xml2js');        

        var builder = new xml2js.Builder({rootName:'payment'});
        var xml = builder.buildObject(this.dadosPagamento);
        this.api.doPayment(xml);
      },      
      error: err=>{
        this.erroCartao = true;
        this.blockUI.stop();
      }      
  });
  }

  getParcelas(){  
  this.dadosPagamento.items.item.amount = '69.90';
  if (this.brandOk){    
    this.PagSeguroDirectPayment.getInstallments({
    amount:  (69.90*this.dadosPagamento.items.item.quantity),              
    brand: this.brand,
    maxInstallmentNoInterest: 3,
    success: response => { 
      let key : any;          
      this.parcelamento = [];
      console.log(response);
      for (key in response.installments){        
        for (let key2 in response.installments[key]){          
            if (!this.parcelaDef)
              this.parcelaDef = response.installments[key][key2];

            this.parcelamento.push(response.installments[key][key2]);
          }        
      }
    },
    error: response => {	console.log( response)	}
    });
    console.log(this.parcelamento);
  }
}

verificaJuros(juros){
  let str = 'com juros';
  if (juros) 
    str = 'sem juros'; 

  return str;
}

verificaZero(valor){
  return parseFloat(valor.toString()).toFixed(2);

}

titularFields(evt){  
  this.isTitular = evt.toElement.checked;

  if (this.isTitular){
    this.dadosPagamento.creditCard.holder.phone.areaCode = this.dadosPagamento.sender.phone.areaCode;
    this.dadosPagamento.creditCard.holder.documents.document.value = this.cpf;
    this.dadosPagamento.creditCard.holder.name = this.dadosPagamento.sender.name;
    this.dadosPagamento.creditCard.holder.phone.number = this.dadosPagamento.sender.phone.number;
    this.dadosPagamento.creditCard.holder.birthDate = this.aniversario;
  } else {
    this.dadosPagamento.creditCard.holder.phone.areaCode = '';
    this.dadosPagamento.creditCard.holder.documents.document.value = '';    
    this.dadosPagamento.creditCard.holder.name = '';    
    this.dadosPagamento.creditCard.holder.phone.number = '';
    this.dadosPagamento.creditCard.holder.birthDate = '';
    this.aniversario2 = '';
  }
  
}

getTotal(){  
  return (parseFloat(this.dadosPagamento.items.item.quantity)*69.90).toFixed(2);
}

email(){
  this.api.sendEmail(this.dadosPagamento.sender);
}

scrollUp(){
  window.scroll(0,0);
}

}
